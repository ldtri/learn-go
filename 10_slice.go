package main

import "fmt"

func main() {
	fmt.Println("Slice")
	var slice []int
	fmt.Println(slice)

	// Khai bao va khoi tao slice
	var slice1 = []int {1, 2, 3, 4}
	fmt.Println(slice1)

	// tao mot slice tu 1 array
	var arr = [4]int {5, 6, 7, 8}
	slice2 := arr[1:2] // array[1] -> array[3-1=2]: array[2]
	fmt.Println(slice2)

	slice3 := arr[:]
	fmt.Println(slice3)

	slice4 := arr[2:]
	fmt.Println(slice4)

	slice5 := arr[:3]
	fmt.Println(slice5)

	var slice6 = []int {1, 2, 3, 4, 5, 6, 7, 8, 9}
	fmt.Println(slice6)
	slice7 := slice6
	fmt.Println(slice7)

	slice8 := slice6[1:]
	fmt.Println(slice8)

	// slice => reference type
	var arr1 = [5]int {1, 2, 3, 4, 5}
	slice9 := arr1[:]
	slice9[0] = 223
	fmt.Println(arr1, slice9)

	// length va capacity cua slice
	countries := [...]string {"US", "UK", "AU", "VN", "FR"}
	slice10 := countries[1:3]
	fmt.Println(slice10, len(slice10), cap(slice10))

	// len: so luong phan tu cua slice
	// cap: so luong phan tu cua underlying array bat dau tu vi tri start khi ma slice duoc tao

	// make, copy, append
	// 1.make
	slice11 := make([]int, 2, 5)
	fmt.Println(slice11, len(slice11), cap(slice11))

	slice12 := make([]int, 2)
	fmt.Println(slice12, len(slice12), cap(slice12))

	// 2. append
	slice13 := make([]int, 0)
	slice13 = append(slice13, 3)
	fmt.Println(slice13)

	// 3. copy
	src := []string {"A", "B", "C", "D"}
	dest := make([]string, 2)
	slice14 := copy(dest, src)
	fmt.Println(slice14, dest)

	// delete item with index = 1
	src = append(src[:1], src[2:]...)
	fmt.Println(src, len(src), cap(src))
}