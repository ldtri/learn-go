package main

import "fmt"

func addItem(item int, list ...int) {
	// 100, 200, 300 -> int[] {100, 200, 300}
	list = append(list, item)
	fmt.Println(list)
}

func change(list ...int) {
	list[0] = 999
}

func main()  {
	fmt.Println("Variadic Functions")
	addItem(11, 100, 200, 300)

	slice := []int {3, 4, 5}
	addItem(2, slice...)
	change(slice...)
	fmt.Println(slice)
}