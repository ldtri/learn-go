package main

import "fmt"

func main()  {
	fmt.Println("Map")
	// Khai bao kieu nay thi zero value khong phai la nil
	var myMap = make(map[string]int)
	fmt.Println(myMap)
	if myMap == nil {
		fmt.Println("myMap is nil")
	} else {
		fmt.Println("myMap is not nil")
	}

	fmt.Println()
	// Khai bao kieu nay thi zero value la nil
	var myMap1 map[string]int
	fmt.Println(myMap1)

	if myMap1 == nil {
		fmt.Println("myMap1 is nil")
	}

	fmt.Println()
	myMap2 := map[string]int {
		"key1": 1,
		"key2": 2,
		"key3": 3,
	}
	fmt.Println(myMap2)
	for pos, val := range myMap2 {
		fmt.Println(pos, val)
	}

	// them 1 phan tu vao map
	myMap2["key4"] = 4
	myMap2["key5"] = 5
	fmt.Println(myMap2)

	// update value
	myMap2["key1"] = 22
	fmt.Println(myMap2)

	// delete 1 phan tu trong map
	delete(myMap2, "key2")
	fmt.Println(myMap2)

	// length cua map
	fmt.Println(len(myMap2))
	fmt.Println()

	// Map la reference type
	myMap3 := myMap2
	fmt.Println(myMap3)
	myMap3["key1"] = 333
	fmt.Println(myMap2)
	fmt.Println(myMap3)

	// truy cap 1 phan tu trong map
	value, found := myMap2["key100"]
	if found {
		fmt.Println("Found")
	} else {
		fmt.Println("Not Found with key100")
	}
	fmt.Println(value)

	// trong map khong co toan tu == co nghia la khong the so sanh 2 map
}
