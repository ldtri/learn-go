package main

import "fmt"

func main()  {
	fmt.Println("Pointer")
	a := 100
	var pointer *int
	pointer = &a
	fmt.Println(pointer)
	fmt.Printf("%T", pointer)

	fmt.Println()
	p2 := new(int)
	p2 = &a
	fmt.Println(p2)
	fmt.Printf("%T", p2)

	// zero value
	fmt.Println()
	var pointer2 *int
	pointer3 := new(int)
	fmt.Println(pointer2)
	fmt.Println(pointer3)

	// can thiep vao value cua pointer
	a2 := 100
	var p4 *int
	p4 = &a2
	fmt.Println(a2)
	*p4 = 99 // <> a2 := 99
	fmt.Println(p4)
	fmt.Println(a2)

	// demo pointer -> array
	array := [3]int {1, 2, 3}
	var p5 *[3]int
	p5 = &array
	fmt.Println(array)
	fmt.Println(p5)

	// cal method
	c := 999
	var p7 *int = &c
	fmt.Println(c)
	fmt.Println(p7)
	applyPointer(p7)
	fmt.Println(c)
	fmt.Println(p7)
}

func applyPointer(pointer *int) {
	*pointer = 7677
}