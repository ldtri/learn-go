package main

import "fmt"

func main()  {
	var number int
	number = 10
	fmt.Println(number)

	var number1 int = 11
	fmt.Println(number1)

	// Type inference
	var email = "trild@gmail.com"
	fmt.Println(email)

	// Khai bao nhieu bien cung kieu du lieu
	var a, b int
	a = 11
	b = 12
	fmt.Println(a)
	fmt.Println(b)

	var a1, b1 int = 13, 14
	fmt.Println(a1)
	fmt.Println(b1)

	var a2, b2 = 15, 15
	fmt.Println(a2)
	fmt.Println(b2)

	// Khai bao nhieu bien khac kieu du lieu
	var (
		name string
		address string
		age int
	)

	name = "Tri LD"
	address = "Viet Nam"
	age = 27
	fmt.Println(name)
	fmt.Println(address)
	fmt.Println(age)

	var (
		name1 string = "Tri LD 1"
		address1 string = "VN1"
		age1 int = 28
	)
	fmt.Println(name1)
	fmt.Println(address1)
	fmt.Println(age1)

	var name2, address2, age2 = "Tri LD 2", "VN2", 29
	fmt.Println(name2)
	fmt.Println(address2)
	fmt.Println(age2)

	// Short
	language := "Vietnamese"
	fmt.Println(language)
}
