package main

import (
	"fmt"
	"math"
	"math/bits"
)

func main() {
	fmt.Println("[Data type]")
	// bool
	var myBool bool = true
	fmt.Println("bool: ", myBool)

	mySecondBool := false
	fmt.Println("bool: ", mySecondBool)

	// string
	myString := "hello"
	fmt.Println("string: ", myString)

	// integer
	var myInt int = 123
	fmt.Println("int: ", myInt)

	// Int: int8, int16, int 32, int64
	// 1. Range
	// 2. Bits

	// 1.Range Int8: -128 > 127
	fmt.Println(math.MinInt8)
	fmt.Println(math.MaxInt8)

	// 1.Range Int16: -32768 > 32767
	fmt.Println(math.MinInt16)
	fmt.Println(math.MaxInt16)

	// 1.Range Int32: -2147483648 > 2147483647
	fmt.Println(math.MinInt32)
	fmt.Println(math.MaxInt32)

	// 1.Range Int64: -9223372036854775808 > 9223372036854775807
	fmt.Println(math.MinInt64)
	fmt.Println(math.MaxInt64)

	// 2 Bits
	fmt.Println("=========")
	fmt.Println(bits.OnesCount8(math.MaxUint8))
	fmt.Println(bits.OnesCount16(math.MaxUint16))
	fmt.Println(bits.OnesCount32(math.MaxUint32))
	fmt.Println(bits.OnesCount64(math.MaxUint64))

	// Uint: so nguyen duong
	fmt.Println("Uint =========")
	var myUint uint = 9
	fmt.Println("Uint: ", myUint)

	// bytes:
	fmt.Println("Byte =========")
	var myByte byte = math.MaxUint8
	fmt.Println("byte: ", myByte)
	fmt.Printf("%T", myByte)

	fmt.Println("=========")
	var aByte byte = 'A'
	fmt.Printf("%X\n", aByte)

	// float
	fmt.Println("Float=========")
	var myFloat float64 = 1.001
	fmt.Println(myFloat)

	// complex: so phuc
	fmt.Println("complex=========")
	var z1 complex64 = 10 + 1i
	var z2 complex64 = 15 + 2i
	fmt.Println(z1 + z2)

	// Rune
	fmt.Println("Rune=========")

}