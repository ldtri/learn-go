package main

import "fmt"

func main()  {
	number := 9
	if number == 10 {
		fmt.Println("number = 10")
	}

	if number < 10 {
		fmt.Println("number < 10")
	} else {
		fmt.Println("number = 10")
	}

	if a := 100; a > 100 {
		fmt.Println("a > 100")
	} else {
		fmt.Println("a == 100")
	}

	number1 := 12
	switch number1 {
	case 1, 2, 3:
		fmt.Println("number = 1")
	case 11:
		fmt.Println("number = 11")
	default:
		fmt.Println("unknown")
	}

	number2 := 23
	switch {
	case number2 < 23:
		fmt.Println("number < 23")
	default:
		fmt.Println("number > 23")
	}

	fmt.Println("=====")
	number3 := 23
	switch number3 {
	case 25:
		fmt.Println("number = 25")
	case 23:
		if number3 == 23 {
			goto handleNumberEqual23
		}
		fmt.Println("number = 23")
		handleNumberEqual23:
			fmt.Println("number for case = 23")
	case 12:
		fmt.Println("number = 12")
		fallthrough
	case 14:
		fmt.Println("number = 14")
		fallthrough
	default:
		fmt.Println("number > 23")
	}
}
