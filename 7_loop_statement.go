package main

import "fmt"

func main()  {
	// Loops
	// for init; condition; post
	for i := 1; i < 10; i++ {
		//if i == 4 {
		//	break
		//}
		if i == 5 {
			continue
		} else if i == 7 {
			continue
		}
		fmt.Println(i)
	}
	fmt.Println("out of loop")

	// while
	j := 0
	for ; j < 10; {
		fmt.Println(j)
		j += 2
	}

	j2 := 0
	for j2 < 10 {
		fmt.Println(j2)
		j2 += 2
	}

	//// infinite loop
	//for {
	//	fmt.Println("infinite loop")
	//}

	// multiple init, condition and post
	for n, m := 1, 2; n < 10 && m < 10; n, m = n + 1, m + 1{
		fmt.Println(n)
		fmt.Println(m)
	}
}
