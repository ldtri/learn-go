package main

import "fmt"

func Chao() {
	fmt.Println("Xin Chao")
}

func XinChao(name string)  {
	fmt.Println("Hello", name)
}

func Greeting(name string) string {
	result := fmt.Sprintf("Hello %s", name)
	return result
}

func Square(a int, b int) int {
	return a * b
}

// RectInfo Multiple return values
func RectInfo(w, h int) (int, int, int) {
	area := w * h
	return w, h, area
}

// NamedRectInfo Named return values
func NamedRectInfo(w, h int) (width int, height int, isSquare bool)  {
	isSquare = w == h
	return w, h, isSquare
}

func main() {
	Chao()
	XinChao("irT")
	fmt.Println(Square(10, 12))
	fmt.Println(Greeting("World"))
	w, h, area := RectInfo(100, 200)
	fmt.Println("width =", w)
	fmt.Println("height =", h)
	fmt.Println("area =", area)

	w1, h1, isSquare := NamedRectInfo(15, 15)
	if isSquare {
		fmt.Println("This is square")
	} else {
		fmt.Println("width =", w1)
		fmt.Println("height =", h1)
	}
}