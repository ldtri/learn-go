package main

import "fmt"

func main()  {
	// khai bao
	var myArr [4]int
	fmt.Println(myArr)

	// gan gia tri
	myArr[0] = 1
	myArr[1] = 2
	myArr[2] = 3
	myArr[3] = 4
	fmt.Println(myArr)

	// khai bao array co khoi tao gia tri
	myArr1 := [3]int {3, 4, 5}
	fmt.Println(myArr1)

	myArr2 := [3]int {5}
	fmt.Println(myArr2)

	myArr3 := [...]string {"A", "B", "C", "D"}
	fmt.Println(myArr3, len(myArr3))

	for i := 0; i < len(myArr3); i ++ {
		fmt.Println(myArr3[i])
	}

	// array la value type khong phai reference type
	countries := [...]string {"VN", "US", "UK"}
	copyCountries := countries

	fmt.Println(countries)
	fmt.Println(copyCountries)

	copyCountries[0] = "SG"
	fmt.Println(countries)
	fmt.Println(copyCountries)

	j := 0
	for j < len(copyCountries) {
		fmt.Println(copyCountries[j])
		j++
	}

	for idx, val := range copyCountries {
		fmt.Printf("index=%d value=%s", idx, val)
		fmt.Println()
	}

	for _, val := range copyCountries {
		fmt.Printf("value=%s", val)
		fmt.Println()
	}

	// mang hai chieu
	matrix := [4][2]int{
		{1, 2},
		{3, 4},
		{5, 6},
		{7, 8},
	}
	fmt.Println(matrix)
	for i := 0; i < 4; i++ {
		for j := 0; j < 2; j++ {
			fmt.Print(matrix[i][j], " ")
		}
		fmt.Println()
	}
}
