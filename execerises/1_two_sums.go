package main

import (
	"fmt"
	"reflect"
)


func TwoSum(nums []int, target int) []int {
	for i := 0; i < len(nums); i++ {
		for j := i + 1; j < len(nums); j++ {
			if nums[i] + nums[j] == target {
				return []int {i, j}
			}
		}
	}
	return []int{}
}

func main() {
	rs1 := TwoSum([]int {2,7,11,15}, 9)

	if !reflect.DeepEqual(rs1, []int {0, 1}) {
		fmt.Println("Wrong at rs1")
	} else {
		fmt.Println("OK at rs1")
	}

	rs2 := TwoSum([]int {3,2,4}, 6)
	if !reflect.DeepEqual(rs2, []int {1, 2}) {
		fmt.Println("Wrong at rs2")
	} else {
		fmt.Println("OK at rs2")
	}
}