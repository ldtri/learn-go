package main

import (
	"fmt"
	"math"
)

func reverse(x int) int {
	if x >= math.MaxInt32 || x <= math.MinInt32 {
		return 0
	}

	rs := 0
	n := x
	if n < 0 {
		n *= -1
	}
	for n > 0 {
		remainder := n % 10
		rs *= 10
		rs += remainder
		n /= 10
	}
	if x < 0 {
		rs *= -1
	}

	if rs >= math.MaxInt32 || rs <= math.MinInt32 {
		return 0
	}

	return rs
}

func main()  {
	fmt.Println(reverse(123))
	fmt.Println(reverse(1534236469))
}
