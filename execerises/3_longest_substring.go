package main

import "fmt"

/*
Given a string s, find the length of the longest substring
without repeating characters.
*/
func lengthOfLongestSubstring(s string) int {
	m, max, left := make(map[rune]int), 0, 0
	for idx, c := range s {
		if _, okay := m[c]; okay == true && m[c] >= left {
			if idx-left > max {
				max = idx - left
			}
			left = m[c] + 1
		}
		m[c] = idx
	}
	if len(s) - left > max {
		max = len(s) - left
	}
	fmt.Println(max)
	return max
}

func main()  {
	lengthOfLongestSubstring("abcabcbb")
	lengthOfLongestSubstring("bbbbb")
	lengthOfLongestSubstring("pwwkew")
	lengthOfLongestSubstring("")
}
